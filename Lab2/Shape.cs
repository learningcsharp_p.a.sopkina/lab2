﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    abstract class Shape
    {
        internal List<CPoint> _points;
        abstract public double[] GetLengthSides();
        abstract public double GetPerimeter();
        abstract public double[] GetAnglehSides();

        public Shape(List<CPoint> points)
        {
            _points = points;
        }

    }
}
