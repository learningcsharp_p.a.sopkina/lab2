﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    internal class Triangle : Shape
    {
        public Triangle(List<CPoint> points) : base(points)
        {
            if (_points.Count != 3)
            {
                throw new Exception("Не 3 стороны");
            }

            if (CPoint.IsPointOnVector(_points[0], _points[1], _points[2])) throw new Exception("Все точки на одно прямо");
        }

        public override double[] GetAnglehSides()
        {
            List<double> angle = new List<double>();
            angle.Add(CPoint.GetAngle3Points(_points[0], _points[1], _points[2]));
            angle.Add(CPoint.GetAngle3Points(_points[1], _points[0], _points[2]));
            angle.Add(CPoint.GetAngle3Points(_points[2], _points[0], _points[1]));
            return angle.ToArray();
        }

        public override double[] GetLengthSides()
        {
            List<double> length = new List<double>();
            length.Add(CPoint.GetLenghtVector(_points[0], _points[1]));
            length.Add(CPoint.GetLenghtVector(_points[1], _points[2]));
            length.Add(CPoint.GetLenghtVector(_points[0], _points[2]));
            return length.ToArray();
        }

        public override double GetPerimeter()
        {
            double perimeter = 0;

            perimeter += CPoint.GetLenghtVector(_points[0], _points[1]);
            perimeter += CPoint.GetLenghtVector(_points[1], _points[2]);
            perimeter += CPoint.GetLenghtVector(_points[0], _points[2]);

            return perimeter;
        }
    }
}
