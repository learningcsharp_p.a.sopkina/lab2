﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    internal class Square : Shape
    {
        public Square(List<CPoint> points) : base(points)
        {
            if (_points.Count != 4)
            {
                throw new Exception("Не 4 стороны");
            }
            if(Math.Round(CPoint.GetAngle3Points(_points[0], _points[1], _points[3])) != 90) throw new Exception("Угол квадрата не 90");
            if(Math.Round(CPoint.GetAngle3Points(_points[1], _points[2], _points[0])) != 90) throw new Exception("Угол квадрата не 90");
            if(Math.Round(CPoint.GetAngle3Points(_points[2], _points[1], _points[3])) != 90) throw new Exception("Угол квадрата не 90");
            if(Math.Round(CPoint.GetAngle3Points(_points[3], _points[2], _points[0])) != 90) throw new Exception("Угол квадрата не 90");
        }

        public override double[] GetAnglehSides()
        {
            List<double> angle = new List<double>();
            angle.Add(90);
            angle.Add(90);
            angle.Add(90);
            angle.Add(90);
            return angle.ToArray();
        }

        public override double[] GetLengthSides()
        {
            List<double> length = new List<double>();
            length.Add(CPoint.GetLenghtVector(_points[0], _points[1]));
            length.Add(CPoint.GetLenghtVector(_points[1], _points[2]));
            length.Add(CPoint.GetLenghtVector(_points[2], _points[3]));
            length.Add(CPoint.GetLenghtVector(_points[0], _points[3]));
            return length.ToArray();
        }

        public override double GetPerimeter()
        {
            double perimeter = 0;

            perimeter += CPoint.GetLenghtVector(_points[0], _points[1]);
            perimeter += CPoint.GetLenghtVector(_points[1], _points[2]);
            perimeter += CPoint.GetLenghtVector(_points[2], _points[3]);
            perimeter += CPoint.GetLenghtVector(_points[0], _points[3]);

            return perimeter;
        }
    }
}
