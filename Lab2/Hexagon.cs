﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    internal class Hexagon : Shape
    {
        public Hexagon(List<CPoint> points) : base(points)
        {
            if (_points.Count != 6)
            {
                throw new Exception("Не 6 сторон");
            }

            if (CPoint.IsPointOnVector(_points[0], _points[1], _points[2])) throw new Exception("Все точки на одно прямо");
            if (CPoint.IsPointOnVector(_points[1], _points[2], _points[3])) throw new Exception("Все точки на одно прямо");
            if (CPoint.IsPointOnVector(_points[2], _points[3], _points[4])) throw new Exception("Все точки на одно прямо");
            if (CPoint.IsPointOnVector(_points[4], _points[5], _points[0])) throw new Exception("Все точки на одно прямо");
            if (CPoint.IsPointOnVector(_points[5], _points[0], _points[1])) throw new Exception("Все точки на одно прямо");
        }

        public override double[] GetAnglehSides()
        {
            List<double> angle = new List<double>();
            angle.Add(CPoint.GetAngle3Points(_points[1], _points[2], _points[0]));
            angle.Add(CPoint.GetAngle3Points(_points[2], _points[1], _points[3]));
            angle.Add(CPoint.GetAngle3Points(_points[3], _points[2], _points[4]));
            angle.Add(CPoint.GetAngle3Points(_points[4], _points[3], _points[5]));
            angle.Add(CPoint.GetAngle3Points(_points[5], _points[4], _points[0]));
            angle.Add(CPoint.GetAngle3Points(_points[0], _points[5], _points[1]));
            return angle.ToArray();
        }

        public override double[] GetLengthSides()
        {
            List<double> length = new List<double>();
            length.Add(CPoint.GetLenghtVector(_points[0], _points[1]));
            length.Add(CPoint.GetLenghtVector(_points[1], _points[2]));
            length.Add(CPoint.GetLenghtVector(_points[2], _points[3]));
            length.Add(CPoint.GetLenghtVector(_points[3], _points[4]));
            length.Add(CPoint.GetLenghtVector(_points[4], _points[5]));
            length.Add(CPoint.GetLenghtVector(_points[0], _points[5]));
            return length.ToArray();
        }

        public override double GetPerimeter()
        {
            double perimeter = 0;

            perimeter += CPoint.GetLenghtVector(_points[0], _points[1]);
            perimeter += CPoint.GetLenghtVector(_points[1], _points[2]);
            perimeter += CPoint.GetLenghtVector(_points[2], _points[3]);
            perimeter += CPoint.GetLenghtVector(_points[3], _points[4]);
            perimeter += CPoint.GetLenghtVector(_points[4], _points[5]);
            perimeter += CPoint.GetLenghtVector(_points[0], _points[5]);

            return perimeter;
        }
    }
}
