﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class CPoint
    {
        public static double GetLenghtVector(CPoint p1, CPoint p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }
        public static double GetAngle3Points(CPoint p1, CPoint p2, CPoint p3)
        {
            return Math.Acos(((p2.X - p1.X) * (p3.X - p1.X) + (p2.Y - p1.Y) * (p3.Y - p1.Y))/(GetLenghtVector(p1,p2)*GetLenghtVector(p1,p3)));
        }
        public static bool IsPointOnVector(CPoint p1, CPoint p2, CPoint p3)
        {
            double ab = GetLenghtVector(p1, p2);
            double ac = GetLenghtVector(p1, p3);
            double bc = GetLenghtVector(p2, p3);
            return Math.Abs(ac+bc - ab)<0.0001;
        }
        public double X { get; set; }
        public double Y { get; set; }
        public CPoint(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
