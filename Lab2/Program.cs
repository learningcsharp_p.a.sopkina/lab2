﻿namespace Lab2
{
    class Program
    {
        static void Main()
        {
            Triangle triangle = new Triangle(new List<CPoint>
            {
                new CPoint(-1, 3),
                new CPoint(2, 6),
                new CPoint(0, 0)
            });
            Console.WriteLine(triangle.GetPerimeter());
            Console.WriteLine(triangle.GetLengthSides()[0]);
            Console.WriteLine(triangle.GetAnglehSides()[0]);

            Square square = new Square(new List<CPoint>
            {
                new CPoint(-1, 3),
                new CPoint(2, 6),
                new CPoint(0, 0),
                new CPoint(-10, 20)
            });
            Console.WriteLine(square.GetPerimeter());
            Console.WriteLine(square.GetLengthSides()[0]);
            Console.WriteLine(square.GetAnglehSides()[0]);

            Hexagon hexagon = new Hexagon(new List<CPoint>
            {
                new CPoint(-1, 3),
                new CPoint(2, 6),
                new CPoint(0, 0),
                new CPoint(-10, 30),
                new CPoint(24, 20),
                new CPoint(10, 50)
            });
            Console.WriteLine(hexagon.GetPerimeter());
            Console.WriteLine(hexagon.GetLengthSides()[0]);
            Console.WriteLine(hexagon.GetAnglehSides()[0]);

        }
    }
}